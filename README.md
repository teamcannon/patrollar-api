## Description

Patrollar API project.

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start:api:dev
$ yarn run start:notification:dev
$ yarn run start:detector:dev
```