import { Inject } from '@nestjs/common';
import { getTenantModelToken } from '../utils/tenancy.utils';

/**
 * Get the instance of the tenant model object
 *
 * @param model any
 */
export const InjectTenancyModel = (model: string) =>
  Inject(getTenantModelToken(model));
