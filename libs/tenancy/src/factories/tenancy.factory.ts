import { Provider } from '@nestjs/common';
import { getTenantModelToken } from '../utils/tenancy.utils';
import { TENANT_CONTEXT } from '../tenancy.constants';
import { getConnectionToken, ModelDefinition } from '@nestjs/mongoose';
import { Connection } from 'mongoose';

export const createTenancyProviders = (
  definitions: ModelDefinition[],
): Provider[] => {
  const providers: Provider[] = [];

  for (const definition of definitions) {
    // Extract the definition data
    const { name, schema, collection } = definition;

    // Creating Models with connections attached
    providers.push({
      provide: getTenantModelToken(name),
      useFactory(connection: Connection, tenantId: string) {
        const model = connection.model(name, schema, collection);
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return model?.byTenant?.(tenantId) ?? model;
      },
      inject: [getConnectionToken(), TENANT_CONTEXT],
    });
  }

  // Return the list of providers mapping
  return providers;
};
