/**
 * Get tenant model name formatted
 *
 * @export
 * @param {string} model
 * @returns
 */
export function getTenantModelToken(model: string) {
  return `${model}Model`;
}
