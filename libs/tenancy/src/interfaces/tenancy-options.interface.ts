import { ModuleMetadata, Type } from '@nestjs/common/interfaces';

/**
 * Options for synchronous setup
 *
 * @export
 * @interface TenancyModuleOptions
 */
export interface TenancyModuleOptions extends Record<string, any> {
  /**
   * The path to the tenant inside the request. Example: "headers['X-TENANT-ID']"
   */
  tenantPath?: string;

  /**
   * Default tenant to be used.
   */
  defaultTenant?: string;
}

/**
 * For creating options dynamically
 *
 * To use this the class implementing `TenancyOptionsFactory` should
 * implement the method `createTenancyOptions` under it.
 *
 * @export
 * @interface TenancyOptionsFactory
 */
export interface TenancyOptionsFactory {
  createTenancyOptions(): Promise<TenancyModuleOptions> | TenancyModuleOptions;
}

/**
 * Options for asynchronous setup
 *
 * @export
 * @interface TenancyModuleAsyncOptions
 */
export interface TenancyModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  useExisting?: Type<TenancyOptionsFactory>;
  useClass?: Type<TenancyOptionsFactory>;
  useFactory?: (
    ...args: any[]
  ) => Promise<TenancyModuleOptions> | TenancyModuleOptions;
  inject?: any[];
}
