import {
  BadRequestException,
  DynamicModule,
  Global,
  Module,
  Provider,
  Scope,
} from '@nestjs/common';
import { Type } from '@nestjs/common/interfaces';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import {
  TenancyModuleAsyncOptions,
  TenancyModuleOptions,
  TenancyOptionsFactory,
} from './interfaces/tenancy-options.interface';
import { TENANT_CONTEXT, TENANT_MODULE_OPTIONS } from './tenancy.constants';

@Global()
@Module({})
export class TenancyCoreModule {
  static register(options: TenancyModuleOptions): DynamicModule {
    /* Module options */
    const tenancyModuleOptionsProvider = {
      provide: TENANT_MODULE_OPTIONS,
      useValue: { ...options },
    };

    /* Tenant Context */
    const tenantContextProvider = this.createTenantContextProvider();

    const providers = [tenancyModuleOptionsProvider, tenantContextProvider];

    return {
      module: TenancyCoreModule,
      providers,
      exports: providers,
    };
  }

  static registerAsync(options: TenancyModuleAsyncOptions): DynamicModule {
    /* Tenant Context */
    const tenantContextProvider = this.createTenantContextProvider();

    /* Asyc providers */
    const asyncProviders = this.createAsyncProviders(options);

    const providers = [...asyncProviders, tenantContextProvider];

    return {
      module: TenancyCoreModule,
      imports: options.imports,
      providers: providers,
      exports: providers,
    };
  }

  private static getTenant(
    req: Request,
    moduleOptions: TenancyModuleOptions,
  ): string {
    if (!moduleOptions) {
      throw new BadRequestException(`Tenant options are mandatory`);
    }
    const { tenantPath, defaultTenant } = moduleOptions;

    if (defaultTenant) {
      return defaultTenant;
    }

    return this.getTenantFromRequest(req, tenantPath);
  }

  private static getTenantFromRequest(req: Request, pathFromRequest: string) {
    const tenantId = this.resolve<string | undefined>(pathFromRequest, req);

    // Validate if tenant id is present
    if (this.isEmpty(tenantId)) {
      throw new BadRequestException(`${pathFromRequest} is not supplied`);
    }

    return tenantId;
  }

  private static createTenantContextProvider(): Provider {
    return {
      provide: TENANT_CONTEXT,
      scope: Scope.REQUEST,
      useFactory: (req: Request, moduleOptions: TenancyModuleOptions) =>
        this.getTenant(req, moduleOptions),
      inject: [REQUEST, TENANT_MODULE_OPTIONS],
    };
  }

  private static createAsyncProviders(
    options: TenancyModuleAsyncOptions,
  ): Provider[] {
    if (options.useExisting || options.useFactory) {
      return [this.createAsyncOptionsProvider(options)];
    }

    const useClass = options.useClass as Type<TenancyOptionsFactory>;

    return [
      this.createAsyncOptionsProvider(options),
      {
        provide: useClass,
        useClass,
      },
    ];
  }

  private static createAsyncOptionsProvider(
    options: TenancyModuleAsyncOptions,
  ): Provider {
    if (options.useFactory) {
      return {
        provide: TENANT_MODULE_OPTIONS,
        useFactory: options.useFactory,
        inject: options.inject || [],
      };
    }

    const inject = [
      (options.useClass || options.useExisting) as Type<TenancyOptionsFactory>,
    ];

    return {
      provide: TENANT_MODULE_OPTIONS,
      useFactory: async (optionsFactory: TenancyOptionsFactory) =>
        await optionsFactory.createTenancyOptions(),
      inject,
    };
  }

  private static isEmpty(obj: any) {
    return !obj || !Object.keys(obj).some((x) => obj[x] !== void 0);
  }

  private static resolve<T>(
    path: string | Array<string>,
    obj: unknown,
    separator = '.',
  ): T {
    const properties = Array.isArray(path) ? path : path.split(separator);
    return properties.reduce((prev, curr) => prev && prev[curr], obj as T);
  }
}
