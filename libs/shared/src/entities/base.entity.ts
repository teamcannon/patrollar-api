import {
  Field,
  ID,
  InputType,
  InterfaceType,
  ObjectType,
  registerEnumType,
} from '@nestjs/graphql';

export enum Sorting {
  ASCENDING = 1,
  DESCENDING = -1,
}

registerEnumType(Sorting, {
  name: 'Sorting',
  description: 'The supported sorting types.',
  valuesMap: {
    ASCENDING: {
      description: 'Sort ascending.',
    },
    DESCENDING: {
      description: 'Sort descending.',
    },
  },
});

@InterfaceType()
export abstract class BaseEntity {
  @Field(() => ID, { description: 'Randomly generated id' })
  id: string;
}

@ObjectType({ isAbstract: true })
@InputType({ isAbstract: true })
export abstract class DateFilter {
  _GTE: Date;
  _LTE: Date;
}
