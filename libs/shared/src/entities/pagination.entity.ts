import { Field, ID, InputType, Int } from '@nestjs/graphql';
import { BaseEntity } from '@patrollar/shared/entities/base.entity';

@InputType()
export class PaginationArgs extends BaseEntity {
  @Field(() => ID, { description: 'Last entity id.', nullable: true })
  id;

  @Field(() => Int)
  limit = 10;
}
