import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { UserContextInterface } from '../interface/user-context.interface';

const UserContext = createParamDecorator<string | undefined>(
  (
    propertyName: 'userId' | 'tenantId' | undefined,
    context: ExecutionContext,
  ): UserContextInterface | string => {
    const { user: userContext } =
      GqlExecutionContext.create(context).getContext();

    return propertyName ? userContext[propertyName] : userContext;
  },
);

export { UserContext };
