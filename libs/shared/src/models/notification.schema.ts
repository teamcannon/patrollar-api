import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import MongoTenant from 'mongo-tenant';

@Schema()
export class Notification extends Document {
  @Prop()
  cameraId: string;
  @Prop()
  tenantId: string;
  @Prop()
  detectionId: string;
  @Prop()
  message: string;
  @Prop()
  timestamp: Date;
  @Prop()
  path: string;
  @Prop({ type: Map })
  usersNotified: Map<string, boolean>;
}

export const NotificationSchema =
  SchemaFactory.createForClass(Notification).plugin(MongoTenant);
