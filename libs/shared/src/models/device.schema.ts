import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import MongoTenant from 'mongo-tenant';

export enum DeviceType {
  MOBILE = 'MOBILE',
  WEB = 'WEB',
  DESKTOP = 'DESKTOP',
}

@Schema()
export class Device extends Document {
  @Prop()
  userId: string;
  @Prop()
  tenantId: string;
  @Prop()
  fcmToken: string;
  @Prop({ default: () => Date.now() })
  timestamp: Date;
  @Prop({ enum: DeviceType })
  application: DeviceType;
}

export const DeviceSchema =
  SchemaFactory.createForClass(Device).plugin(MongoTenant);
