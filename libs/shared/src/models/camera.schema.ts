import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import MongoTenant from 'mongo-tenant';

@Schema()
export class Camera extends Document {
  @Prop()
  isArmed: boolean;
  @Prop()
  name: string;
  @Prop()
  url: string;
  @Prop()
  tenantId: string;
}

export const CameraSchema =
  SchemaFactory.createForClass(Camera).plugin(MongoTenant);
