import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Tenant extends Document {
  @Prop()
  timestamp: Date;
  @Prop()
  name: string;
}

export const TenantSchema = SchemaFactory.createForClass(Tenant);
