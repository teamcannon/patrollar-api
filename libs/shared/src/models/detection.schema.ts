import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import MongoTenant from 'mongo-tenant';

@Schema()
export class Detection extends Document {
  @Prop({ default: () => Date.now() })
  timestamp: Date;
  @Prop()
  cameraId: string;
  @Prop()
  path: string;
  @Prop()
  tenantId: string;
}

export const DetectionSchema =
  SchemaFactory.createForClass(Detection).plugin(MongoTenant);
