import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId } from 'mongoose';

@Schema()
export class User extends Document {
  @Prop()
  email: string;

  @Prop()
  username: string;

  @Prop()
  phoneNumber: string;

  @Prop()
  password: string;

  @Prop({ default: false })
  emailConfirmed: boolean;

  @Prop({ default: [] })
  tenants: string[];
}

export const UserSchema = SchemaFactory.createForClass(User);
