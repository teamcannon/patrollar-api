export class ObjectUtils {
  static lowerCaseKeys(obj: Record<string, unknown>) {
    return Object.entries(obj).reduce(
      (accumulator, [key, value]) =>
        Object.assign(accumulator, {
          [key.toLowerCase()]: value,
        }),
      {},
    );
  }

  static get<T>(
    path: string | Array<string>,
    obj: unknown,
    separator = '.',
  ): T {
    const properties = Array.isArray(path) ? path : path.split(separator);
    return properties.reduce((prev, curr) => prev && prev[curr], obj as T);
  }
}
