import { Injectable, Logger } from '@nestjs/common';
import {
  Transporter,
  createTransport,
  SendMailOptions,
  SentMessageInfo,
} from 'nodemailer';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class EmailService {
  private readonly context = 'EmailService';
  private mailer: Transporter;

  constructor(private readonly configService: ConfigService) {
    this.mailer = createTransport({
      host: this.configService.get<string>('SMTP_HOST'),
      port: +this.configService.get<number>('SMTP_PORT'),
      auth: {
        user: this.configService.get<string>('SMTP_USERNAME'),
        pass: this.configService.get<string>('SMTP_PASSWORD'),
      },
    });
  }

  public async send(mailOptions: SendMailOptions): Promise<SentMessageInfo> {
    Logger.log(`Sending email to ${mailOptions.to}`, this.context);
    return this.mailer.sendMail(mailOptions);
  }
}
