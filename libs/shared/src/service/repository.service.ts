import { Injectable, Logger } from '@nestjs/common';
import {
  Aggregate,
  Document,
  EnforceDocument,
  FilterQuery,
  Model,
  Query,
} from 'mongoose';
import queryParser from './graphql-mongo-query-parser';

interface IPagination {
  id?: string;
  limit: number;
}

@Injectable()
export class RepositoryService {
  private readonly context = 'RepositoryService';
  private readonly queryParser = queryParser({ $exists: '$exists' });

  async create<T extends Document, E>(model: Model<T>, value: E): Promise<T> {
    RepositoryService.assertTenant(model);
    const newValue = new model(value);
    return newValue.save();
  }

  async findOne<T extends Document>(model: Model<T>, query: any): Promise<T> {
    RepositoryService.assertTenant(model);
    Logger.log(
      `Executing findOne on ${model.modelName} with query: ${JSON.stringify(
        query,
      )}`,
      this.context,
    );
    return model.findOne(query).exec();
  }

  async findAll<T extends Document>(
    model: Model<T>,
    query: any = {},
    sort: any = null,
    pagination: IPagination = null,
  ): Promise<T[]> {
    RepositoryService.assertTenant(model);
    const parsedQuery = this.queryParser(query) ?? {};
    const finalQuery = this.getPaginatedQuery<T>(parsedQuery, sort, pagination);

    Logger.log(
      `Executing findAll on ${model.modelName} with query: ${JSON.stringify(
        finalQuery,
      )}`,
      this.context,
    );

    const cursor = model.find(finalQuery);
    this.applySort(cursor, sort);
    this.applyLimit(cursor, pagination);

    return cursor.exec();
  }

  async findOneAndDelete<T extends Document>(
    model: Model<T>,
    query: FilterQuery<T>,
  ): Promise<T> {
    RepositoryService.assertTenant(model);
    Logger.log(
      `Executing findByIdAndDelete on ${
        model.modelName
      } with id: ${JSON.stringify(query)}`,
      this.context,
    );
    return model.findOneAndDelete(query).exec();
  }

  async findByIdAndUpdate<E, T extends Document>(
    model: Model<T>,
    id: string,
    entity: E,
  ): Promise<T> {
    RepositoryService.assertTenant(model);
    Logger.log(
      `Executing findByIdAndUpdate on ${model.modelName} with id: ${id}`,
      this.context,
    );
    return model
      .findByIdAndUpdate(id, entity, { returnOriginal: false })
      .exec();
  }

  async updateMany<E, T extends Document>(
    model: Model<T>,
    query: FilterQuery<any>,
    update: E,
  ): Promise<number> {
    RepositoryService.assertTenant(model);
    Logger.log(
      `Executing updateMany on ${model.modelName} with query: ${JSON.stringify(
        query,
      )}`,
      this.context,
    );
    const { modifiedCount } = await model.updateMany(query, update).exec();
    return modifiedCount;
  }

  async aggregate<T extends Document, E>(
    model: Model<T>,
    query: unknown[],
  ): Promise<Aggregate<Array<E>>> {
    RepositoryService.assertTenant(model);
    Logger.log(
      `Executing aggregate on ${model.modelName} with query: ${JSON.stringify(
        query,
      )}`,
      this.context,
    );
    return model.aggregate(query).exec();
  }

  private applySort<T extends Document>(
    cursor: Query<
      EnforceDocument<T, Record<string, unknown>>[],
      EnforceDocument<T, Record<string, unknown>>,
      Record<string, unknown>,
      T
    >,
    query,
  ): void {
    if (query) {
      cursor.sort(query);
    }
  }

  private getPaginatedQuery<T extends Document>(
    query: FilterQuery<T>,
    sort: Record<string, number>,
    pagination: IPagination,
  ): FilterQuery<T> {
    const [sortDirection = 1] = Object.values(sort || {});
    const operator = sortDirection === -1 ? '$lt' : '$gt';

    return pagination?.id
      ? Object.assign(query, { _id: { [operator]: pagination.id } })
      : query;
  }

  private applyLimit<T extends Document>(
    cursor: Query<
      EnforceDocument<T, Record<string, unknown>>[],
      EnforceDocument<T, Record<string, unknown>>,
      Record<string, unknown>,
      T
    >,
    pagination: IPagination,
  ) {
    if (pagination) {
      cursor.limit(pagination.limit);
    }
  }

  static assertTenant<T>(model: Model<T>) {
    const { options } = model.schema.path('tenantId') ?? {};

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if (options?.required && !model?.getTenantId?.()) {
      throw new Error(`Missing tenant on "${model.modelName}"`);
    }
  }
}
