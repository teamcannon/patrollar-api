export interface CameraHeartbeatInterface {
  cameraId: string;
  isAlive: boolean;
}
