export interface UserContextInterface {
  userId: string;
  tenantId: string;
}
