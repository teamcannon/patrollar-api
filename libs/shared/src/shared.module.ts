import { Module, OnModuleInit } from '@nestjs/common';
import { RepositoryService } from './service/repository.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { EmailService } from '@patrollar/shared/service/email.service';
import admin from 'firebase-admin';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: `.env.${process.env.NODE_ENV}`,
    }),
  ],
  providers: [RepositoryService, EmailService],
  exports: [RepositoryService, EmailService],
})
export class SharedModule implements OnModuleInit {
  public constructor(private readonly configService: ConfigService) {}

  onModuleInit() {
    const privateKey = this.configService
      .get<string>('GOOGLE_PRIVATE_KEY')
      .replace(/\\n/g, '\n');

    const projectId = this.configService.get<string>('GOOGLE_PROJECT_ID');
    const clientEmail = this.configService.get<string>('GOOGLE_CLIENT_EMAIL');
    const storageBucket = this.configService.get<string>('GOOGLE_BUCKET_URL');
    admin.initializeApp({
      credential: admin.credential.cert({
        privateKey,
        projectId,
        clientEmail,
      }),
      storageBucket,
    });
  }
}
