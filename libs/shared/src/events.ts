export const SERVICE_EVENTS = {
  NOTIFICATION: 'notification',
  ARM_CAMERA: 'arm_camera',
  ARM_CAMERAS: 'arm_cameras',
  DETECTION_HEARTBEAT: 'detection_heartbeat',
};

export const WEBSOCKET_EVENTS = {
  CAMERA_CHANGE: 'camera_change',
};
