import json
import time
import os

import cv2
from google.cloud import storage
from google.oauth2 import service_account


def nparray_to_image(img):
    return cv2.imencode('.jpg', img, [int(cv2.IMWRITE_JPEG_QUALITY), 50])[1].tostring()


def current_milli_time():
    return round(time.time() * 1000)


def upload_blob(camera, outputDetections, image):
    try:
        credentials_dict = {
            "type": "service_account",
            "project_id": os.environ['GOOGLE_PROJECT_ID'],
            "private_key": os.environ['GOOGLE_PRIVATE_KEY'],
            "client_email": os.environ['GOOGLE_CLIENT_EMAIL'],
            "token_uri": "https://oauth2.googleapis.com/token"
        }
        credentials = service_account.Credentials.from_service_account_info(credentials_dict)
        client = storage.Client(project=os.environ['GOOGLE_PROJECT_ID'], credentials=credentials)
        bucket = client.bucket(os.environ['GOOGLE_BUCKET_URL'])
        path = "{}/{}/{}.jpg".format(camera["tenantId"], camera["_id"], current_milli_time())
        bucket.blob(path).upload_from_string(nparray_to_image(image), content_type="image/jpeg")
        print(json.dumps({
            "payload": {
                "path": path,
                "cameraId": camera["_id"],
                "detections": outputDetections,
            },
            "event": "detection"
        }))
        # return bucket.blob(target_key).public_url

    except Exception as e:
        print(e)


def detection_upload(camera, outputDetections, image):
    # Create run loop for this thread and block until completion
    upload_blob(camera, outputDetections, image)

