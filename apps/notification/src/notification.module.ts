import { Module } from '@nestjs/common';
import { NotificationController } from './notification.controller';
import { NotificationService } from './notification.service';
import {
  Notification,
  NotificationSchema,
} from '@patrollar/shared/models/notification.schema';
import { SharedModule } from '@patrollar/shared';
import { Device, DeviceSchema } from '@patrollar/shared/models/device.schema';
import { TenancyModule } from '@patrollar/tenancy/tenancy.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get('MONGO_URI'),
      }),
      inject: [ConfigService],
    }),
    TenancyModule.forRoot({
      tenantPath: 'data.userContext.tenantId',
    }),
    TenancyModule.forFeature([
      { name: Notification.name, schema: NotificationSchema },
      { name: Device.name, schema: DeviceSchema },
    ]),
    SharedModule,
  ],
  controllers: [NotificationController],
  providers: [NotificationService],
})
export class NotificationModule {}
