import { Controller } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { SERVICE_EVENTS } from '@patrollar/shared/events';
import { Detection } from '@patrollar/shared/models/detection.schema';
import { Notification } from '@patrollar/shared/models/notification.schema';

@Controller()
export class NotificationController {
  constructor(private readonly notificationService: NotificationService) {}

  @EventPattern(SERVICE_EVENTS.NOTIFICATION)
  sendNotification(
    @Payload('detection') payload: Detection,
  ): Promise<Notification> {
    return this.notificationService.create(payload);
  }
}
