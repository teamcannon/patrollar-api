import { Injectable } from '@nestjs/common';
import { Notification } from '@patrollar/shared/models/notification.schema';
import { Model } from 'mongoose';
import { RepositoryService } from '@patrollar/shared';
import { Detection } from '@patrollar/shared/models/detection.schema';
import { Device } from '@patrollar/shared/models/device.schema';
import uniqBy from 'lodash/uniqBy';
import { messaging, storage } from 'firebase-admin';
import { InjectTenancyModel } from '@patrollar/tenancy/decorators/tenancy.decorator';

@Injectable()
export class NotificationService {
  constructor(
    @InjectTenancyModel(Notification.name)
    private notificationModel: Model<Notification>,
    @InjectTenancyModel(Device.name) private deviceModel: Model<Device>,
    private readonly repositoryService: RepositoryService,
  ) {}

  private async getDevices(): Promise<Device[]> {
    return this.repositoryService.findAll(this.deviceModel);
  }

  private getUsersToNotify(devices: Device[]): Record<string, boolean> {
    const userDevices: Device[] = uniqBy(devices, 'userId');
    return userDevices.reduce((accumulator, { userId }) => {
      Object.assign(accumulator, { [userId]: false });
      return accumulator;
    }, {} as Record<string, boolean>);
  }

  async create(detection: Detection) {
    const devices = await this.getDevices();
    const usersNotified = this.getUsersToNotify(devices);

    const notification = {
      timestamp: detection.timestamp,
      cameraId: detection.cameraId,
      detectionId: detection._id,
      path: detection.path,
      message: 'Movement has been detected.',
      usersNotified,
    };

    await this.sendPushNotification(devices, detection);

    return this.repositoryService.create<Notification, typeof notification>(
      this.notificationModel,
      notification,
    );
  }

  private async sendPushNotification(devices: Device[], detection: Detection) {
    const fcmTokens = devices.map(({ fcmToken }) => fcmToken);
    const expires = new Date().getTime() + 60 * 60 * 60;
    const [url] = await storage().bucket().file(detection.path).getSignedUrl({
      action: 'read',
      expires,
    });
    const payload: messaging.MessagingPayload = {
      notification: {
        title: 'Intrusion!',
        body: 'Movement has been detected.',
        sound: 'default',
        image: url,
      },
      data: {
        detection: JSON.stringify(detection),
      },
    };

    // TODO catch errors and remove expired device entries (expired/invalid fcmTokens)
    await messaging().sendToDevice(fcmTokens, payload, { priority: 'high' });
  }
}
