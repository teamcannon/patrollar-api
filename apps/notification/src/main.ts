import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { Logger } from '@nestjs/common';
import { NotificationModule } from './notification.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(NotificationModule, {
    transport: Transport.REDIS,
    options: {
      retryAttempts: 10,
      retryDelay: 3000,
      url: process.env.REDIS_URI,
    },
  });

  await app.listen();
  Logger.log('Microservice is listening...');
}

(async () => bootstrap())();
