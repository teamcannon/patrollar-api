import { Controller, Logger } from '@nestjs/common';
import { DetectorService } from './detector.service';
import { EventPattern } from '@nestjs/microservices';
import { SERVICE_EVENTS } from '@patrollar/shared/events';
import { Camera } from '@patrollar/shared/models/camera.schema';
import { CameraHeartbeatInterface } from '@patrollar/shared/interface/camera-heartbeat.interface';

@Controller()
export class DetectorController {
  private readonly logger = new Logger('DetectorController');

  constructor(private readonly detectorService: DetectorService) {}

  @EventPattern({
    cmd: SERVICE_EVENTS.ARM_CAMERA,
    tenantId: process.env.DEFAULT_TENANT,
  })
  armCamera(camera: Camera): void {
    this.logger.log(`Arm camera ${camera._id} with ${camera.isArmed}`);
    return this.detectorService.armCamera(camera);
  }

  @EventPattern({
    cmd: SERVICE_EVENTS.ARM_CAMERAS,
    tenantId: process.env.DEFAULT_TENANT,
  })
  armCameras(cameras: Camera[]): void {
    this.logger.log(`Arm all cameras`);
    return this.detectorService.armCameras(cameras);
  }

  @EventPattern({
    cmd: SERVICE_EVENTS.DETECTION_HEARTBEAT,
    tenantId: process.env.DEFAULT_TENANT,
  })
  heartbeat(cameraId: string): Promise<CameraHeartbeatInterface[]> {
    return this.detectorService.heartbeat(cameraId);
  }
}
