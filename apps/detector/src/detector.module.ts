import { CacheModule, Module, OnModuleInit } from '@nestjs/common';
import { DetectorController } from './detector.controller';
import { DetectorService } from './detector.service';
import { SharedModule } from '@patrollar/shared';
import { MongooseModule } from '@nestjs/mongoose';
import { Camera, CameraSchema } from '@patrollar/shared/models/camera.schema';
import {
  Detection,
  DetectionSchema,
} from '@patrollar/shared/models/detection.schema';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { MessageHandlerService } from './message-handler.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TenancyModule } from '@patrollar/tenancy/tenancy.module';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get('MONGO_URI'),
      }),
      inject: [ConfigService],
    }),
    TenancyModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        defaultTenant: configService.get('DEFAULT_TENANT'),
      }),
      inject: [ConfigService],
    }),
    TenancyModule.forFeature([
      { name: Camera.name, schema: CameraSchema },
      { name: Detection.name, schema: DetectionSchema },
    ]),
    ClientsModule.register([
      {
        name: 'NOTIFICATION_SERVICE',
        transport: Transport.REDIS,
        options: {
          retryAttempts: 10,
          retryDelay: 3000,
          url: process.env.REDIS_URI,
        },
      },
    ]),
    CacheModule.register(),
    SharedModule,
    ConfigModule,
  ],
  controllers: [DetectorController],
  providers: [DetectorService, MessageHandlerService],
})
export class DetectorModule {}
