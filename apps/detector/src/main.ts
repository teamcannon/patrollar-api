import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { Logger } from '@nestjs/common';
import { DetectorModule } from './detector.module';
import { DetectorService } from './detector.service';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(DetectorModule, {
    transport: Transport.REDIS,
    options: {
      retryAttempts: 20,
      retryDelay: 10000,
      url: process.env.REDIS_URI,
    },
  });

  const detectorService = await app.resolve(DetectorService);
  await detectorService.startup();

  await app.listen();

  Logger.log(`Detector microservice is listening...`);
}

(async () => bootstrap())();
