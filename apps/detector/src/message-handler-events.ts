export enum MessageHandlerEvents {
  DETECTION = 'detection',
  HEARTBEAT = 'heartbeat',
}

export function getCacheKey(event: MessageHandlerEvents, cameraId: string) {
  switch (event) {
    case MessageHandlerEvents.DETECTION:
      return `${MessageHandlerEvents.DETECTION}/${cameraId}`;
    case MessageHandlerEvents.HEARTBEAT:
      return `${MessageHandlerEvents.HEARTBEAT}/${cameraId}`;
    default:
      throw new Error(`Unrecognised event: ${event}`);
  }
}
