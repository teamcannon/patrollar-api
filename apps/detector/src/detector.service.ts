import { CACHE_MANAGER, Inject, Injectable, Logger } from '@nestjs/common';
import { ChildProcess, spawn } from 'child_process';
import { join } from 'path';
import { Model } from 'mongoose';
import { Camera } from '@patrollar/shared/models/camera.schema';
import { RepositoryService } from '@patrollar/shared';
import { MessageHandlerService } from './message-handler.service';
import { InjectTenancyModel } from '@patrollar/tenancy/decorators/tenancy.decorator';
import { getCacheKey, MessageHandlerEvents } from './message-handler-events';
import { Cache } from 'cache-manager';
import { ConfigService } from '@nestjs/config';
import { CameraHeartbeatInterface } from '@patrollar/shared/interface/camera-heartbeat.interface';

const detectorForks: { [key: string]: ChildProcess } = {};

@Injectable()
export class DetectorService {
  private readonly logger = new Logger('DetectorService');
  private readonly detectorHeartbeatThreshold: number;

  constructor(
    private readonly repositoryService: RepositoryService,
    private readonly messageHandlerService: MessageHandlerService,
    private readonly configService: ConfigService,
    @InjectTenancyModel(Camera.name) private cameraModel: Model<Camera>,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {
    this.detectorHeartbeatThreshold =
      +this.configService.get<number>('DETECTOR_HEARTBEAT_THRESHOLD') * 1000;
  }

  async startup(): Promise<void> {
    const cameras = await this.repositoryService.findAll(this.cameraModel);
    return cameras.forEach((camera) => this.startDetector(camera));
  }

  armCamera(camera: Camera): void {
    const detectorFork = detectorForks[camera._id];
    detectorFork.stdin.write(JSON.stringify(camera));
    detectorFork.stdin.write('\n');
  }

  armCameras(cameras: Camera[]): void {
    return cameras.forEach((camera) => this.armCamera(camera));
  }

  heartbeat(cameraId?: string): Promise<CameraHeartbeatInterface[]> {
    const cameraIds = cameraId ? [cameraId] : Object.keys(detectorForks);

    return Promise.all(
      cameraIds.map(async (id) => {
        const isAlive = await this.cameraHeartbeat(id);
        return {
          isAlive,
          cameraId: id,
        };
      }),
    );
  }

  private async cameraHeartbeat(cameraId: string): Promise<boolean> {
    const result = await this.cacheManager.get<string | undefined>(
      getCacheKey(MessageHandlerEvents.HEARTBEAT, cameraId),
    );
    this.logger.log(`heartbeat camera: ${cameraId} result: ${result}`);

    if (!result) {
      return false;
    }

    return (
      new Date().getTime() - new Date(result).getTime() <=
      this.detectorHeartbeatThreshold
    );
  }

  private startDetector(camera: Camera): void {
    this.logger.log(`Starting detection fork: ${camera._id} - ${camera.name}`);
    const options = [
      '-u',
      join(process.cwd(), 'yolov5/detect.py'),
      '--weights',
      'yolov5m.pt',
      '--camera_input',
      JSON.stringify(camera),
      '--conf-thres',
      '0.75',
      '--nosave',
      // '--device',
      // 'cpu',
    ];
    const detectorFork = spawn('python', options);
    detectorFork.stdout.on('data', (message: unknown) =>
      this.messageHandlerService.handle(message),
    );
    detectorFork.stderr.on('data', (message: unknown) => {
      this.logger.log(`got data stderr ${camera._id}`, message.toString());
    });
    detectorFork.stderr.on('error', (message: unknown) => {
      this.logger.log(`got error  stderr ${camera._id}`, message);
    });

    detectorFork.on('exit', (code, signal) => {
      this.logger.error(
        `[RESTARTING] got exit ${camera._id} exit ${code} signal ${signal}`,
      );
      if (signal) {
        this.startDetector(camera);
      }
    });

    Object.assign(detectorForks, { [camera._id]: detectorFork });
  }

  private stopDetector(camera: Camera): void {
    this.logger.log(`Killing detector for ${camera._id} - ${camera.name}`);
    detectorForks[camera._id].kill();
    delete detectorForks[camera._id];
  }

  private onError(message: unknown) {
    this.logger.error('[LADI] onError', message.toString());
  }
}
