import { SERVICE_EVENTS } from '@patrollar/shared/events';
import { Model } from 'mongoose';
import { Detection } from '@patrollar/shared/models/detection.schema';
import { CACHE_MANAGER, Inject, Injectable, Logger } from '@nestjs/common';
import { RepositoryService } from '@patrollar/shared';
import { ClientProxy } from '@nestjs/microservices';
import { Cache } from 'cache-manager';
import { ConfigService } from '@nestjs/config';
import { firstValueFrom } from 'rxjs';
import { InjectTenancyModel } from '@patrollar/tenancy/decorators/tenancy.decorator';
import { getCacheKey, MessageHandlerEvents } from './message-handler-events';

interface Message {
  event: string;
}

interface DetectionMessage extends Message {
  payload: { cameraId: string; path: string };
}

interface HeartbeatMessage extends Message {
  payload: { cameraId: string };
}

@Injectable()
export class MessageHandlerService {
  private readonly logger = new Logger('MessageHandlerService');
  private readonly detectionThrottleAmount: number;

  constructor(
    private readonly repositoryService: RepositoryService,
    private readonly configService: ConfigService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    @InjectTenancyModel(Detection.name)
    private detectionModel: Model<Detection>,
    @Inject('NOTIFICATION_SERVICE') private client: ClientProxy,
  ) {
    this.detectionThrottleAmount =
      +this.configService.get<number>('DETECTION_THROTTLE');
  }

  async handle<T = unknown>(message: T): Promise<void> {
    const data = message.toString();
    try {
      const parsedData: DetectionMessage = JSON.parse(data);
      if (parsedData.event === MessageHandlerEvents.DETECTION) {
        return this.saveDetection(parsedData);
      }
      if (parsedData.event === MessageHandlerEvents.HEARTBEAT) {
        return this.handleHeartbeat(parsedData);
      }
    } catch (e) {
      this.logger.log(data);
    }
  }

  private async saveDetection({ payload }: DetectionMessage): Promise<void> {
    const cacheKey = getCacheKey(
      MessageHandlerEvents.DETECTION,
      payload.cameraId,
    );
    const result = await this.cacheManager.get<string | undefined>(cacheKey);
    if (result) {
      return;
    }
    this.logger.log('saving detection');
    await this.cacheManager.set(cacheKey, cacheKey, {
      ttl: this.detectionThrottleAmount,
    });
    const detection = await this.repositoryService.create(
      this.detectionModel,
      payload,
    );
    this.logger.log(
      `sending ${SERVICE_EVENTS.NOTIFICATION} to notification service.`,
    );
    await firstValueFrom(
      this.client.emit(SERVICE_EVENTS.NOTIFICATION, {
        detection,
        userContext: { tenantId: detection.tenantId },
      }),
    );
  }

  private async handleHeartbeat({ payload }: HeartbeatMessage): Promise<void> {
    const cacheKey = getCacheKey(
      MessageHandlerEvents.HEARTBEAT,
      payload.cameraId,
    );

    this.logger.log(`Heartbeat: ${payload.cameraId}`);

    await this.cacheManager.set(cacheKey, new Date().toISOString(), {
      ttl: 0,
    });
  }
}
