import { Field, ID, InputType, ObjectType, PickType } from '@nestjs/graphql';
import { BaseEntity } from '@patrollar/shared/entities/base.entity';

@InputType({ isAbstract: true })
@ObjectType({ isAbstract: true })
class CommonEntity {
  /**
   * User's email.
   */
  email: string;
  /**
   * User's phone number.
   */
  phoneNumber: string;
  /**
   * Tenants a user has access to.
   */
  tenants: string[];
}

@ObjectType({
  implements: () => [BaseEntity],
  description: 'Entity representing a detection',
})
export class UserEntity extends CommonEntity implements BaseEntity {
  @Field(() => ID, { description: 'Randomly generated id' })
  id: string;
}

@InputType()
export class UserInput extends PickType(CommonEntity, [
  'email',
  'phoneNumber',
]) {
  /**
   * User's password
   */
  password: string;
  /**
   * User's confirmation password
   */
  confirmPassword: string;
}
