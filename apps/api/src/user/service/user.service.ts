import {
  Injectable,
  PreconditionFailedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { Model } from 'mongoose';
import { User } from '@patrollar/shared/models/user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { RepositoryService } from '@patrollar/shared';
import { UserInput } from '../entity/user.entity';
import { compare, hash } from 'bcrypt';

Injectable();
export class UserService {
  constructor(
    @InjectModel(User.name) private model: Model<User>,
    private readonly repositoryService: RepositoryService,
  ) {}

  async create(input: UserInput) {
    input.password = await this.getHashedPassword(input.password);

    const user = await this.findByEmail(input.email);

    if (user) {
      throw new UnprocessableEntityException('Email already in use');
    }

    return this.repositoryService.create(this.model, input);
  }

  async findById(id: string) {
    return this.repositoryService.findOne(this.model, { _id: id });
  }

  async findByEmail(email: string) {
    return this.repositoryService.findOne(this.model, { email });
  }

  async validateCredentials(user: User, password: string): Promise<boolean> {
    return compare(password, user.password);
  }

  async resetPassword(email: string, newPassword: string): Promise<User> {
    const user = await this.findByEmail(email);
    if (!user) {
      throw new PreconditionFailedException('User does not exist.');
    }
    const hashedPassword = await this.getHashedPassword(newPassword);

    return this.repositoryService.findByIdAndUpdate(this.model, user.id, {
      password: hashedPassword,
    });
  }

  async confirmRegistration(userId: string) {
    return this.repositoryService.findByIdAndUpdate(this.model, userId, {
      emailConfirmed: true,
    });
  }

  private getHashedPassword(password: string): Promise<string> {
    return hash(password, 10);
  }
}
