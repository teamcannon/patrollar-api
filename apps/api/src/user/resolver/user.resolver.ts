import { Query, Resolver } from '@nestjs/graphql';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UseGuards } from '@nestjs/common';
import { JWTGuard } from '../../auth/guard/jwt.guard';
import { UserEntity } from '../entity/user.entity';
import { User } from '@patrollar/shared/models/user.schema';
import { UserService } from '../service/user.service';
import { UserContext } from '@patrollar/shared/decorators/request.decorator';

@Resolver(() => UserEntity)
@UseGuards(JWTGuard)
export class UserResolver {
  constructor(
    @InjectModel(User.name) private tenantModel: Model<User>,
    private readonly userService: UserService,
  ) {}

  @Query(() => UserEntity)
  async me(@UserContext('userId') userId: string) {
    return this.userService.findById(userId);
  }
}
