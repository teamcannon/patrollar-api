import { Field, ID, InputType, ObjectType } from '@nestjs/graphql';
import { BaseEntity } from '@patrollar/shared/entities/base.entity';

@ObjectType({ isAbstract: true })
@InputType({ isAbstract: true })
abstract class CommonEntity {
  /**
   * The tenant's name
   */
  name: string;
}

@ObjectType({
  implements: () => [BaseEntity],
  description: 'Entity representing a detection',
})
export class TenantEntity extends CommonEntity implements BaseEntity {
  @Field(() => ID, { description: 'Randomly generated id' })
  id: string;
}

@InputType()
export class TenantInput extends CommonEntity {}
