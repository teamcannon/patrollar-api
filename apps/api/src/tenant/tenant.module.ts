import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SharedModule } from '@patrollar/shared/shared.module';
import { TenantResolver } from './resolver/tenant.resolver';
import { Tenant, TenantSchema } from '@patrollar/shared/models/tenant.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Tenant.name, schema: TenantSchema }]),
    SharedModule,
  ],
  providers: [TenantResolver],
})
export class TenantModule {}
