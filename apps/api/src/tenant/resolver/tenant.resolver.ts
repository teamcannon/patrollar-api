import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { TenantEntity, TenantInput } from '../entity/tenant.entity';
import { Tenant } from '@patrollar/shared/models/tenant.schema';
import { RepositoryService } from '@patrollar/shared';
import { UseGuards } from '@nestjs/common';
import { JWTGuard } from '../../auth/guard/jwt.guard';

@Resolver(() => TenantEntity)
@UseGuards(JWTGuard)
export class TenantResolver {
  constructor(
    @InjectModel(Tenant.name) private tenantModel: Model<Tenant>,
    private readonly repositoryService: RepositoryService,
  ) {}

  @Query(() => TenantEntity)
  async tenant(@Args('id') id: string) {
    return this.repositoryService.findOne(this.tenantModel, { _id: id });
  }

  @Query(() => [TenantEntity])
  async tenants() {
    return this.repositoryService.findAll(this.tenantModel);
  }

  @Mutation(() => TenantEntity)
  async createTenant(@Args('input') input: TenantInput) {
    return this.repositoryService.create(this.tenantModel, input);
  }
}
