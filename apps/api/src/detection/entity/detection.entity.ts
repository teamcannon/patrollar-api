import { Field, ID, InputType, ObjectType } from '@nestjs/graphql';
import {
  BaseEntity,
  DateFilter,
  Sorting,
} from '@patrollar/shared/entities/base.entity';

@ObjectType({ isAbstract: true })
@InputType({ isAbstract: true })
abstract class CommonEntity {
  /**
   * Timestamp of detection
   */
  timestamp?: Date;
  /**
   * Camera id
   */
  cameraId: string;
  /**
   * Path to detection in the storage bucket
   */
  path: string;
}

@ObjectType({
  implements: () => [BaseEntity],
  description: 'Entity representing a detection',
})
export class DetectionEntity extends CommonEntity implements BaseEntity {
  @Field(() => ID, { description: 'Randomly generated id' })
  id: string;
}

@InputType()
export class DetectionInput extends CommonEntity {}

@InputType()
export class DetectionWhereInput implements BaseEntity {
  @Field(() => ID, { description: 'Randomly generated id', nullable: true })
  id: string;

  @Field(() => ID, { description: 'Camera id', nullable: true })
  cameraId?: string;

  timestamp?: DateFilter;
}

@InputType()
export class DetectionSortInput {
  @Field(() => Sorting)
  timestamp: Sorting;
}
