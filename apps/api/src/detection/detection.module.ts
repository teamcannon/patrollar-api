import { Module } from '@nestjs/common';
import { SharedModule } from '@patrollar/shared/shared.module';
import { DetectionResolver } from './resolver/detection.resolver';
import { AuthModule } from '../auth/auth.module';
import {
  Detection,
  DetectionSchema,
} from '@patrollar/shared/models/detection.schema';
import { TenancyModule } from '@patrollar/tenancy/tenancy.module';

@Module({
  imports: [
    SharedModule,
    TenancyModule.forFeature([
      { name: Detection.name, schema: DetectionSchema },
    ]),
    AuthModule,
  ],
  providers: [DetectionResolver],
})
export class DetectionModule {}
