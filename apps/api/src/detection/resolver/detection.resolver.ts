import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { Model } from 'mongoose';
import {
  DetectionEntity,
  DetectionInput,
  DetectionSortInput,
  DetectionWhereInput,
} from '../entity/detection.entity';
import { Detection } from '@patrollar/shared/models/detection.schema';
import { RepositoryService } from '@patrollar/shared';
import { JWTGuard } from '../../auth/guard/jwt.guard';
import { UseGuards } from '@nestjs/common';
import { storage } from 'firebase-admin';
import { PaginationArgs } from '@patrollar/shared/entities/pagination.entity';
import { InjectTenancyModel } from '@patrollar/tenancy/decorators/tenancy.decorator';

@Resolver(() => DetectionEntity)
@UseGuards(JWTGuard)
export class DetectionResolver {
  constructor(
    private readonly repositoryService: RepositoryService,
    @InjectTenancyModel(Detection.name) private model: Model<Detection>,
  ) {}

  @Query(() => DetectionEntity)
  async detection(@Args('id') id: string) {
    return this.repositoryService.findOne(this.model, { _id: id });
  }

  @Query(() => [DetectionEntity])
  async detections(
    @Args({ name: 'where', nullable: true }) query: DetectionWhereInput,
    @Args({ name: 'sort', nullable: true }) sortQuery: DetectionSortInput,
    @Args({ name: 'pagination' }) pagination: PaginationArgs,
  ) {
    return this.repositoryService.findAll(
      this.model,
      query,
      sortQuery,
      pagination,
    );
  }

  @Mutation(() => DetectionEntity)
  async createDetection(@Args('input') input: DetectionInput) {
    return this.repositoryService.create(this.model, input);
  }

  @ResolveField(() => String, { description: 'Signed URL of the image' })
  async url(@Parent() detection: Detection) {
    const expires = new Date().getTime() + 60 * 60 * 1000;
    const [url] = await storage().bucket().file(detection.path).getSignedUrl({
      action: 'read',
      expires,
    });

    return url;
  }
}
