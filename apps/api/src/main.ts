import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(/*{ logger: true }*/),
  );
  const configService = app.get(ConfigService);
  const PORT = +configService.get<number>('PORT') || 3000;
  const HOST = configService.get<string>('HOST') || 'localhost';

  await app.listen(PORT, HOST);
  Logger.log(`Api listening on ${await app.getUrl()}`);
}

(async () => {
  await bootstrap();
})();
