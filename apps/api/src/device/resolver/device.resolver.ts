import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Model } from 'mongoose';
import { RepositoryService } from '@patrollar/shared';
import { UseGuards } from '@nestjs/common';
import { JWTGuard } from '../../auth/guard/jwt.guard';
import {
  DeviceEntity,
  DeviceInput,
  DeviceWhereInput,
} from '../entity/device.entity';
import { Device } from '@patrollar/shared/models/device.schema';
import { UserContext } from '@patrollar/shared/decorators/request.decorator';
import { InjectTenancyModel } from '@patrollar/tenancy/decorators/tenancy.decorator';

@Resolver(() => DeviceEntity)
@UseGuards(JWTGuard)
export class DeviceResolver {
  constructor(
    @InjectTenancyModel(Device.name) private model: Model<Device>,
    private readonly repositoryService: RepositoryService,
  ) {}

  @Query(() => [DeviceEntity])
  async devices(
    @Args({ name: 'where', nullable: true }) query: DeviceWhereInput,
  ) {
    return this.repositoryService.findAll(this.model, query);
  }

  @Mutation(() => DeviceEntity)
  async createDevice(
    @UserContext('userId') userId: string,
    @Args('input') input: DeviceInput,
  ) {
    const device = await this.repositoryService.findOne(this.model, {
      fcmToken: input.fcmToken,
    });
    if (device) {
      return device;
    }
    return this.repositoryService.create(this.model, {
      ...input,
      userId,
    });
  }

  @Mutation(() => DeviceEntity)
  async deleteDevice(@Args('fcmToken') fcmToken: string) {
    return this.repositoryService.findOneAndDelete(this.model, {
      fcmToken,
    });
  }
}
