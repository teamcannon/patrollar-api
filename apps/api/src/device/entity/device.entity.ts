import {
  Field,
  ID,
  InputType,
  ObjectType,
  PickType,
  registerEnumType,
} from '@nestjs/graphql';
import { BaseEntity } from '@patrollar/shared/entities/base.entity';
import { DeviceType } from '@patrollar/shared/models/device.schema';

registerEnumType(DeviceType, {
  name: 'DeviceType',
  description: 'The various device types (client applications.',
  valuesMap: {
    MOBILE: {
      description: 'Refers to the mobile application.',
    },
    WEB: {
      description: 'Refers to the web application.',
    },
    DESKTOP: {
      description: 'Refers to the desktop application.',
    },
  },
});

@ObjectType({ isAbstract: true })
@InputType({ isAbstract: true })
abstract class CommonEntity {
  /**
   * Timestamp of device registration
   */
  timestamp?: Date;
  /**
   * Firebase Cloud Messaging token
   */
  fcmToken: string;

  @Field(() => DeviceType, {
    description: 'Application type (web/mobile/application)',
  })
  application: DeviceType;
}

@ObjectType({
  implements: () => [BaseEntity],
  description: 'Entity representing a detection',
})
export class DeviceEntity extends CommonEntity implements BaseEntity {
  @Field(() => ID, { description: 'Randomly generated id' })
  id: string;
}

@InputType()
export class DeviceInput extends CommonEntity {}

@InputType()
export class DeviceWhereInput extends PickType(DeviceInput, ['fcmToken']) {}
