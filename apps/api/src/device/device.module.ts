import { Module } from '@nestjs/common';
import { SharedModule } from '@patrollar/shared/shared.module';
import { Device, DeviceSchema } from '@patrollar/shared/models/device.schema';
import { DeviceResolver } from './resolver/device.resolver';
import { TenancyModule } from '@patrollar/tenancy/tenancy.module';

@Module({
  imports: [
    TenancyModule.forFeature([{ name: Device.name, schema: DeviceSchema }]),
    SharedModule,
  ],
  providers: [DeviceResolver],
})
export class DeviceModule {}
