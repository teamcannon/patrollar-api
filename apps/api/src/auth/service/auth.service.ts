import {
  Injectable,
  Logger,
  PreconditionFailedException,
  UnauthorizedException,
} from '@nestjs/common';
import { UserService } from '../../user/service/user.service';
import { EmailService } from '@patrollar/shared/service/email.service';
import { UserEntity, UserInput } from '../../user/entity/user.entity';
import { TokenService } from './token.service';

@Injectable()
export class AuthService {
  private readonly context = 'AuthService';
  constructor(
    private readonly userService: UserService,
    private readonly emailService: EmailService,
    private readonly tokenService: TokenService,
  ) {}

  public async login(email: string, password: string) {
    Logger.log(`Attempting to login ${email}`, this.context);
    const user = await this.userService.findByEmail(email.toLowerCase());
    const valid = user
      ? await this.userService.validateCredentials(user, password)
      : false;

    if (!valid) {
      throw new UnauthorizedException('The login is invalid');
    }

    if (!user.emailConfirmed) {
      throw new UnauthorizedException('You have not confirmed your account');
    }

    const token = await this.tokenService.generateAccessToken(user);
    const refreshToken = await this.tokenService.generateRefreshToken(
      user,
      1_209_600_000, // 14 days,
    );

    return {
      user: user as UserEntity,
      token,
      refreshToken,
    };
  }

  public async register(userInput: UserInput) {
    Logger.log(`Attempting to register ${userInput.email}`, this.context);

    const user = await this.userService.create(userInput);
    const registrationToken = await this.tokenService.generateExpiringToken(
      user,
      '4h',
    );

    await this.emailService.send({
      from: 'ladislauandrasi@gmail.com',
      to: user.email,
      subject: '[Patrollar] Email Confirmation',
      html: `<a href="https://api.patrollar.com/auth/confirmRegistration?token=${registrationToken}">Click here to confirm your account.</a>`,
    });

    return user;
  }

  public async refresh(refresh: string) {
    Logger.log('Refreshing token', this.context);

    const { user, token, refreshToken } =
      await this.tokenService.createAccessTokenFromRefreshToken(refresh);

    // TODO maybe remove old refresh token from db
    return {
      user: user as UserEntity,
      token,
      refreshToken,
    };
  }

  public async forgotPassword(email: string) {
    Logger.log(`Forgot password ${email}`, this.context);
    const user = await this.userService.findByEmail(email);
    if (!user) {
      Logger.log(
        `Forgot password ${email} - user does not exist`,
        this.context,
      );
      return 'If the user is registered, an email will be sent.';
    }

    const registrationToken = await this.tokenService.generateExpiringToken(
      user,
      60 * 60,
    );

    await this.emailService.send({
      from: 'ladislauandrasi@gmail.com',
      to: email,
      subject: '[Patrollar] Reset password instructions',
      html: `<a href="https://api.patrollar.com/auth/forgotPassword?token=${registrationToken}">Click here to reset your password.</a>`,
    });

    return 'If the user is registered, an email will be sent.';
  }

  public async resetPassword(
    email: string,
    password: string,
    confirmPassword: string,
  ) {
    if (password !== confirmPassword) {
      throw new PreconditionFailedException('Passwords do not match');
    }

    return this.userService.resetPassword(email, password);
  }

  public async validateRegistration(token: string) {
    const { sub: userId } = await this.tokenService.decodeToken<{
      sub: string;
    }>(token);

    return this.userService.confirmRegistration(userId);
  }

  public async validateForgotPassword(token: string) {
    const { sub: userId } = await this.tokenService.decodeToken<{
      sub: string;
    }>(token);

    return this.userService.findById(userId);
  }
}
