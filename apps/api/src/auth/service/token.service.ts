import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { RefreshToken } from '@patrollar/shared/models/refresh-token.schema';
import { JwtService } from '@nestjs/jwt';
import { User } from '@patrollar/shared/models/user.schema';
import { SignOptions, TokenExpiredError } from 'jsonwebtoken';
import { RefreshTokenService } from './refresh-token.service';
import { UserService } from '../../user/service/user.service';

const BASE_OPTIONS: SignOptions = {
  issuer: 'https://patrollar.com',
  audience: 'https://patrollar.com',
};

export interface RefreshTokenPayload {
  jti: string;
  sub: string;
}

@Injectable()
export class TokenService {
  public constructor(
    private readonly refreshTokenService: RefreshTokenService,
    private readonly jwt: JwtService,
    private readonly userService: UserService,
  ) {}

  public async generateAccessToken(user: User): Promise<string> {
    const opts: SignOptions = {
      ...BASE_OPTIONS,
      subject: String(user.id),
    };

    return this.jwt.signAsync({}, opts);
  }

  public async generateRefreshToken(
    user: User,
    expiresIn: number,
  ): Promise<string> {
    const token = await this.refreshTokenService.createRefreshToken(
      user,
      expiresIn,
    );

    const opts: SignOptions = {
      ...BASE_OPTIONS,
      expiresIn,
      subject: String(user.id),
      jwtid: String(token.id),
    };

    return this.jwt.signAsync({}, opts);
  }

  public async resolveRefreshToken(
    encoded: string,
  ): Promise<{ user: User; token: RefreshToken }> {
    const payload = await this.decodeToken<RefreshTokenPayload>(encoded);
    const token = await this.getStoredTokenFromRefreshTokenPayload(payload);

    if (!token) {
      throw new UnprocessableEntityException('Refresh token not found');
    }

    if (token.isRevoked) {
      throw new UnprocessableEntityException('Refresh token revoked');
    }

    const user = await this.getUserFromRefreshTokenPayload(payload);

    if (!user) {
      throw new UnprocessableEntityException('Refresh token malformed');
    }

    return { user, token };
  }

  public async createAccessTokenFromRefreshToken(
    refresh: string,
  ): Promise<{ token: string; refreshToken: string; user: User }> {
    const { user } = await this.resolveRefreshToken(refresh);

    const token = await this.generateAccessToken(user);
    const refreshToken = await this.generateRefreshToken(
      user,
      1_209_600_000, // 14 days,
    );

    return { user, token, refreshToken };
  }

  public async generateExpiringToken(user: User, expiresIn: number | string) {
    const opts: SignOptions = {
      ...BASE_OPTIONS,
      expiresIn,
      subject: String(user.id),
    };

    return this.jwt.signAsync({}, opts);
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  public async decodeToken<T extends object>(token: string): Promise<T> {
    try {
      return this.jwt.verifyAsync<T>(token);
    } catch (e) {
      if (e instanceof TokenExpiredError) {
        throw new UnprocessableEntityException('Token expired');
      } else {
        throw new UnprocessableEntityException('Token malformed');
      }
    }
  }

  private async getUserFromRefreshTokenPayload(
    payload: RefreshTokenPayload,
  ): Promise<User> {
    const subId = payload.sub;

    if (!subId) {
      throw new UnprocessableEntityException('Refresh token malformed');
    }

    return this.userService.findById(subId);
  }

  private async getStoredTokenFromRefreshTokenPayload(
    payload: RefreshTokenPayload,
  ): Promise<RefreshToken | null> {
    const tokenId = payload.jti;

    if (!tokenId) {
      throw new UnprocessableEntityException('Refresh token malformed');
    }

    return this.refreshTokenService.findTokenById(tokenId);
  }
}
