import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RefreshToken } from '@patrollar/shared/models/refresh-token.schema';
import { User } from '@patrollar/shared/models/user.schema';
import { RepositoryService } from '@patrollar/shared';

@Injectable()
export class RefreshTokenService {
  constructor(
    @InjectModel(RefreshToken.name)
    private readonly refreshTokenModel: Model<RefreshToken>,
    private readonly repositoryService: RepositoryService,
  ) {}

  public async createRefreshToken(
    user: User,
    ttl: number,
  ): Promise<RefreshToken> {
    const expiration = new Date();
    expiration.setTime(expiration.getTime() + ttl);

    const token = {
      userId: user.id,
      isRevoked: false,
      expires: expiration,
    };

    return this.repositoryService.create(this.refreshTokenModel, token);
  }

  public async findTokenById(id: string): Promise<RefreshToken | null> {
    return this.repositoryService.findOne(this.refreshTokenModel, { _id: id });
  }
}
