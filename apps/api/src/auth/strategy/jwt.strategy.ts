import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
import { UserService } from '../../user/service/user.service';
import { FastifyRequest } from 'fastify';
import { UserContextInterface } from '@patrollar/shared/interface/user-context.interface';

interface JWTEncodedProperties {
  sub: string;
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  public constructor(
    private readonly configService: ConfigService,
    private readonly userService: UserService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('JWT_SECRET'),
      passReqToCallback: true,
    });
  }

  async validate(
    req: FastifyRequest,
    { sub: userId }: JWTEncodedProperties,
  ): Promise<UserContextInterface> {
    const tenantId = req?.headers?.['x-tenant-id'] as string;
    const user = await this.userService.findById(userId);

    if (!user.tenants.includes(tenantId)) {
      throw new UnauthorizedException('You are not authorized on this tenant.');
    }
    return { userId, tenantId };
  }
}
