import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';
import { FastifyRequest } from 'fastify';

@Injectable()
export class JWTGuard extends AuthGuard('jwt') {
  getRequest(context: ExecutionContext) {
    return GqlExecutionContext.create(context).getContext<FastifyRequest>();
  }

  handleRequest(err, userContext, info: Error) {
    if (err || info || !userContext) {
      throw err || info || new UnauthorizedException();
    }

    return userContext;
  }
}
