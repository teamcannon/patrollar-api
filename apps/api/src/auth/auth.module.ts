import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RefreshTokenService } from './service/refresh-token.service';
import { UserModule } from '../user/user.module';
import { AuthResolver } from './resolver/auth.resolver';
import { TokenService } from './service/token.service';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JWTGuard } from './guard/jwt.guard';
import { JwtStrategy } from './strategy/jwt.strategy';
import {
  RefreshToken,
  RefreshTokenSchema,
} from '@patrollar/shared/models/refresh-token.schema';
import { SharedModule } from '@patrollar/shared';
import { AuthController } from './controllers/AuthController';
import { AuthService } from './service/auth.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: RefreshToken.name, schema: RefreshTokenSchema },
    ]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => {
        return {
          secret: configService.get<string>('JWT_SECRET'),
          signOptions: {
            expiresIn: '2d',
          },
        };
      },
      inject: [ConfigService],
    }),
    ConfigModule,
    SharedModule,
    UserModule,
  ],
  providers: [
    RefreshTokenService,
    AuthResolver,
    TokenService,
    JwtStrategy,
    JWTGuard,
    AuthService,
  ],
  controllers: [AuthController],
  exports: [JWTGuard],
})
export class AuthModule {}
