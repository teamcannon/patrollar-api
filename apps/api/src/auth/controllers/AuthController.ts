import { Controller, Get, HttpStatus, Req, Res } from '@nestjs/common';
import { FastifyRequest, FastifyReply } from 'fastify';
import { AuthService } from '../service/auth.service';

type FastifyTokenRequest = FastifyRequest<{
  Querystring: { token: string };
}>;

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Get('confirmRegistration')
  async confirmRegistration(
    @Req() request: FastifyTokenRequest,
    @Res() response: FastifyReply,
  ): Promise<FastifyReply> {
    try {
      const { token } = request.query;

      await this.authService.validateRegistration(token);
      return response
        .status(HttpStatus.PERMANENT_REDIRECT)
        .redirect('patrollar://login');
    } catch (e) {
      return response.status(HttpStatus.FORBIDDEN).send(e.message);
    }
  }

  @Get('forgotPassword')
  async forgotPassword(
    @Req() request: FastifyTokenRequest,
    @Res() response: FastifyReply,
  ): Promise<FastifyReply> {
    try {
      const { token } = request.query;

      const { email } = await this.authService.validateForgotPassword(token);
      return response
        .status(HttpStatus.PERMANENT_REDIRECT)
        .redirect(`patrollar://resetPassword?email=${email}`);
    } catch (e) {
      return response.status(HttpStatus.FORBIDDEN).send(e.message);
    }
  }
}
