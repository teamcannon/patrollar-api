import { Field, InputType, ObjectType } from '@nestjs/graphql';
import { UserEntity } from '../../user/entity/user.entity';

@ObjectType()
export class AuthEntity {
  @Field(() => UserEntity)
  user: UserEntity;

  token: string;

  refreshToken: string;
}

@InputType()
export class LoginInput {
  email: string;
  password: string;
}

@InputType()
export class RefreshInput {
  refreshToken: string;
}

@InputType()
export class ForgotPasswordInput {
  email: string;
}

@InputType()
export class ResetPasswordInput {
  email: string;
  password: string;
  confirmPassword: string;
}
