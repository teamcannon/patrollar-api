import { Args, Mutation, Resolver } from '@nestjs/graphql';
import {
  AuthEntity,
  ForgotPasswordInput,
  LoginInput,
  RefreshInput,
  ResetPasswordInput,
} from '../entity/auth.entity';
import { UserEntity, UserInput } from '../../user/entity/user.entity';
import { User } from '@patrollar/shared/models/user.schema';
import { AuthService } from '../service/auth.service';

@Resolver()
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Mutation(() => UserEntity)
  async register(@Args('input') userInput: UserInput): Promise<User> {
    return this.authService.register(userInput);
  }

  @Mutation(() => AuthEntity)
  async login(@Args('input') loginInput: LoginInput): Promise<AuthEntity> {
    const { email, password } = loginInput;
    return this.authService.login(email, password);
  }

  @Mutation(() => AuthEntity)
  public async refresh(
    @Args('input') refreshInput: RefreshInput,
  ): Promise<AuthEntity> {
    const { refreshToken } = refreshInput;
    return this.authService.refresh(refreshToken);
  }

  @Mutation(() => String)
  public async forgotPassword(
    @Args('input') forgotPasswordInput: ForgotPasswordInput,
  ): Promise<string> {
    const { email } = forgotPasswordInput;

    return this.authService.forgotPassword(email);
  }

  @Mutation(() => UserEntity)
  public async resetPassword(
    @Args('input') resetPasswordInput: ResetPasswordInput,
  ): Promise<User> {
    const { email, password, confirmPassword } = resetPasswordInput;

    return this.authService.resetPassword(email, password, confirmPassword);
  }
}
