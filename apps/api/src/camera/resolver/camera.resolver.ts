import {
  Args,
  ID,
  Int,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
  Subscription,
} from '@nestjs/graphql';
import {
  CameraEntity,
  CameraInput,
  DetectorHeartbeat,
  UpdateCameraInput,
} from '../entity/camera.entity';
import { Model } from 'mongoose';
import { Inject, UseGuards } from '@nestjs/common';
import { JWTGuard } from '../../auth/guard/jwt.guard';
import { Camera } from '@patrollar/shared/models/camera.schema';
import { RepositoryService } from '@patrollar/shared';
import { SERVICE_EVENTS, WEBSOCKET_EVENTS } from '@patrollar/shared/events';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom, timeout } from 'rxjs';
import { PubSubEngine } from 'graphql-subscriptions';
import { InjectTenancyModel } from '@patrollar/tenancy/decorators/tenancy.decorator';
import { UserContext } from '@patrollar/shared/decorators/request.decorator';
import { CameraHeartbeatInterface } from '@patrollar/shared/interface/camera-heartbeat.interface';

@Resolver(() => CameraEntity)
@UseGuards(JWTGuard)
export class CameraResolver {
  constructor(
    @InjectTenancyModel(Camera.name) private readonly model: Model<Camera>,
    private readonly repositoryService: RepositoryService,
    @Inject('DETECTOR_SERVICE') private client: ClientProxy,
    @Inject('PUB_SUB') private readonly pubSub: PubSubEngine,
  ) {}

  @Query(() => [CameraEntity])
  async cameras() {
    return this.repositoryService.findAll(this.model);
  }

  @Query(() => CameraEntity)
  async camera(@Args('id') id: string) {
    return this.repositoryService.findOne(this.model, { _id: id });
  }

  @Mutation(() => CameraEntity)
  async createCamera(@Args('input') input: CameraInput) {
    return this.repositoryService.create(this.model, input);
  }

  @Mutation(() => CameraEntity)
  async updateCamera(
    @Args('id') id: string,
    @Args('input') input: UpdateCameraInput,
  ) {
    return this.repositoryService.findByIdAndUpdate(this.model, id, input);
  }

  @Mutation(() => CameraEntity)
  async deleteCamera(@Args('id') id: string) {
    return this.repositoryService.findOneAndDelete(this.model, { id });
  }

  @Mutation(() => Int)
  async armCameras(
    @UserContext('tenantId') tenantId: string,
    @Args({ name: 'id', nullable: true, type: () => ID }) id?: string,
  ): Promise<number> {
    // TODO Refactor this method;
    if (id) {
      const cameraEntity = await this.repositoryService.findOne(this.model, {
        _id: id,
      });

      const camera = await this.repositoryService.findByIdAndUpdate(
        this.model,
        id,
        {
          isArmed: !cameraEntity.isArmed,
        },
      );

      await firstValueFrom(
        this.client.emit({ cmd: SERVICE_EVENTS.ARM_CAMERA, tenantId }, camera),
      );
      await this.pubSub.publish(WEBSOCKET_EVENTS.CAMERA_CHANGE, {
        cameraChange: [camera],
      });

      return 1;
    }
    // TODO maybe avoid having to do a find on this
    const allCameras = await this.repositoryService.findAll(this.model);
    const isArmed = allCameras.every((camera) => camera.isArmed === true);
    const cameras = allCameras.map((camera) => camera.set('isArmed', !isArmed));

    const nModified = await this.repositoryService.updateMany(
      this.model,
      {},
      { isArmed: !isArmed },
    );

    await firstValueFrom(
      this.client.emit({ cmd: SERVICE_EVENTS.ARM_CAMERAS, tenantId }, cameras),
    );
    await this.pubSub.publish(WEBSOCKET_EVENTS.CAMERA_CHANGE, {
      cameraChange: cameras,
    });

    return nModified;
  }

  @ResolveField(() => String)
  async url(@Parent() camera: Camera) {
    const { id } = camera;
    return `https://stream.patrollar.com/play/hls/${id}/index.m3u8`;
  }

  @Subscription(() => [CameraEntity])
  cameraChange() {
    return this.pubSub.asyncIterator<Camera[]>(WEBSOCKET_EVENTS.CAMERA_CHANGE);
  }

  @Query(() => [DetectorHeartbeat], { nullable: true })
  async detectorHeartbeat(
    @UserContext('tenantId') tenantId: string,
    @Args({
      name: 'cameraId',
      nullable: true,
      defaultValue: '',
      type: () => ID,
    })
    cameraId?: string,
  ): Promise<CameraHeartbeatInterface[]> {
    return firstValueFrom(
      this.client
        .send({ cmd: SERVICE_EVENTS.DETECTION_HEARTBEAT, tenantId }, cameraId)
        .pipe(timeout({ first: 5 * 1000 })),
    );
  }
}
