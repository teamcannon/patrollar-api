import { Module } from '@nestjs/common';
import { CameraResolver } from './resolver/camera.resolver';
import { SharedModule } from '@patrollar/shared/shared.module';
import { AuthModule } from '../auth/auth.module';
import { Camera, CameraSchema } from '@patrollar/shared/models/camera.schema';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';
import { PubSub } from 'graphql-subscriptions';
import { TenancyModule } from '@patrollar/tenancy/tenancy.module';

@Module({
  imports: [
    TenancyModule.forFeature([{ name: Camera.name, schema: CameraSchema }]),
    SharedModule,
    AuthModule,
  ],
  providers: [
    ConfigService,
    {
      provide: 'DETECTOR_SERVICE',
      useFactory: (configService: ConfigService) => {
        return ClientProxyFactory.create({
          transport: Transport.REDIS,
          options: {
            retryAttempts: 10,
            retryDelay: 3000,
            url: configService.get<string>('REDIS_URI'),
          },
        });
      },
      inject: [ConfigService],
    },
    {
      provide: 'PUB_SUB',
      useValue: new PubSub(),
    },
    CameraResolver,
  ],
})
export class CameraModule {}
