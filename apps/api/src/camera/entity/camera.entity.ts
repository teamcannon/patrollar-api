import {
  Field,
  ID,
  InputType,
  ObjectType,
  OmitType,
  PartialType,
} from '@nestjs/graphql';
import { BaseEntity } from '@patrollar/shared/entities/base.entity';

@ObjectType({ isAbstract: true })
@InputType({ isAbstract: true })
abstract class CommonEntity {
  /**
   * Boolean value to indicate if the camera is checking for intrusions
   */
  isArmed: boolean;
  /**
   * Name of the camera that will be used for the UI
   */
  name: string;
  /**
   * URL to connect to the camera
   */
  url: string;
}

@ObjectType({
  implements: () => [BaseEntity],
  description: 'Entity representing the Camera',
})
export class CameraEntity extends CommonEntity implements BaseEntity {
  @Field(() => ID, { description: 'Randomly generated id' })
  id: string;
}

@InputType()
export class CameraInput extends CommonEntity {}

@InputType()
export class UpdateCameraInput extends PartialType(
  OmitType(CameraInput, ['isArmed']),
) {}

@ObjectType()
export class DetectorHeartbeat {
  @Field(() => ID, { description: 'Camera Id' })
  cameraId: string;
  @Field(() => Boolean, { description: 'Detector is alive.' })
  isAlive: boolean;
}
