import { Module } from '@nestjs/common';
import { SharedModule } from '@patrollar/shared/shared.module';
import { NotificationResolver } from './resolver/notification.resolver';
import {
  NotificationSchema,
  Notification,
} from '@patrollar/shared/models/notification.schema';
import { TenancyModule } from '@patrollar/tenancy/tenancy.module';

@Module({
  imports: [
    TenancyModule.forFeature([
      { name: Notification.name, schema: NotificationSchema },
    ]),
    SharedModule,
  ],
  providers: [NotificationResolver],
})
export class NotificationModule {}
