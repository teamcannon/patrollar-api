import { Args, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import {
  NotificationEntity,
  NotificationCameraCount,
  NotificationSortArgs,
  NotificationWhereArgs,
} from '../entity/notification.entity';
import { Model } from 'mongoose';
import { UseGuards } from '@nestjs/common';
import { RepositoryService } from '@patrollar/shared';
import { Notification } from '@patrollar/shared/models/notification.schema';
import { UserContext } from '@patrollar/shared/decorators/request.decorator';
import { JWTGuard } from '../../auth/guard/jwt.guard';
import { PaginationArgs } from '@patrollar/shared/entities/pagination.entity';
import { storage } from 'firebase-admin';
import { InjectTenancyModel } from '@patrollar/tenancy/decorators/tenancy.decorator';

@Resolver(() => NotificationEntity)
@UseGuards(JWTGuard)
export class NotificationResolver {
  constructor(
    private readonly repositoryService: RepositoryService,
    @InjectTenancyModel(Notification.name) private model: Model<Notification>,
  ) {}

  @Query(() => [NotificationEntity])
  async notifications(
    @UserContext('userId') userId: string,
    @Args({ nullable: true, name: 'where' }) query: NotificationWhereArgs,
    @Args({ nullable: true, name: 'sort' }) sortQuery: NotificationSortArgs,
    @Args({ name: 'pagination' }) pagination: PaginationArgs,
  ) {
    // TODO maybe rethink this a bit?
    const extendedQuery = Object.assign(query || {}, {
      [`usersNotified.${userId}`]: { $exists: true },
    });

    return this.repositoryService.findAll(
      this.model,
      extendedQuery,
      sortQuery,
      pagination,
    );
  }

  @ResolveField()
  isRead(
    @UserContext('userId') id: string,
    @Parent() notification: Notification,
  ): boolean {
    const { usersNotified } = notification;
    return usersNotified[id] ?? false;
  }

  @ResolveField(() => String)
  async url(@Parent() notification: Notification): Promise<string> {
    const expires = new Date().getTime() + 60 * 60 * 1000;
    const [url] = await storage()
      .bucket()
      .file(notification.path)
      .getSignedUrl({
        action: 'read',
        expires,
      });

    return url;
  }

  @Query(() => [NotificationCameraCount])
  async unreadNotificationCount(@UserContext('userId') id: string) {
    const query = [
      {
        $match: { [`usersNotified.${id}`]: false },
      },
      {
        $group: { _id: '$cameraId', count: { $sum: 1 } },
      },
      {
        $project: { cameraId: '$_id', count: '$count' },
      },
    ];
    return this.repositoryService.aggregate<
      Notification,
      { cameraId: string; count: number }[]
    >(this.model, query);
  }
}
