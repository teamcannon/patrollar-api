import { Field, ID, InputType, ObjectType } from '@nestjs/graphql';
import { BaseEntity, Sorting } from '@patrollar/shared/entities/base.entity';

@ObjectType({ isAbstract: true })
@InputType({ isAbstract: true })
abstract class CommonEntity {
  /**
   * Timestamp of notification
   */
  timestamp: Date;
  /**
   * Camera id
   */
  cameraId: string;
  /**
   * Camera id
   */
  detectionId: string;
  /**
   * Path to notification image in the storage bucket
   */
  url: string;
  /**
   * Notification message
   */
  message: string;
}

@ObjectType({
  implements: () => [BaseEntity],
  description: 'Entity representing a detection',
})
export class NotificationEntity extends CommonEntity implements BaseEntity {
  @Field(() => ID, { description: 'Randomly generated id' })
  id: string;

  /**
   * Indicates if the current notification has been read by the current user
   */
  isRead: boolean;
}

@InputType()
export class NotificationInput extends CommonEntity {}

@InputType()
export class NotificationWhereArgs {
  @Field(() => ID, { description: 'Camera id', nullable: true })
  cameraId?: string;
}

@InputType()
export class NotificationSortArgs {
  @Field(() => Sorting)
  timestamp: Sorting;
}

@ObjectType()
export class NotificationCameraCount {
  @Field(() => ID, { description: 'Camera Id' })
  cameraId: string;

  /**
   * Number of unread notifications for the current user.
   */
  count: number;
}
