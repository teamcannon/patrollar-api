import { Module, UnauthorizedException } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { CameraModule } from './camera/camera.module';
import { SharedModule } from '@patrollar/shared/shared.module';
import { DetectionModule } from './detection/detection.module';
import { TenantModule } from './tenant/tenant.module';
import { NotificationModule } from './notification/notification.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { DeviceModule } from './device/device.module';
import { TenancyModule } from '@patrollar/tenancy/tenancy.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ObjectUtils } from '@patrollar/shared/utils/object.utils';

function getSubscriptionHeaders<T extends Record<string, unknown>>(
  connectionParams: T,
): Record<string, unknown> {
  const headers = ObjectUtils.lowerCaseKeys(connectionParams);

  if (!headers['authorization']) {
    throw new UnauthorizedException('Unauthorized!');
  }

  // this mocks the headers into the right place in the context
  return { headers };
}

@Module({
  imports: [
    GraphQLModule.forRoot({
      // autoSchemaFile can also be true and the schema will be generated in memory
      autoSchemaFile: join(process.cwd(), 'apps/api/src/schema.gql'),
      debug: true,
      introspection: true,
      playground: true,
      installSubscriptionHandlers: true,
      path: '/',
      context: (args) => {
        const { request, connectionParams } = args;
        return connectionParams
          ? getSubscriptionHeaders(connectionParams)
          : request;
      },
      subscriptions: {
        'subscriptions-transport-ws': {
          onConnect: (connectionParams) =>
            getSubscriptionHeaders(connectionParams),
        },
        'graphql-ws': true,
      },
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get('MONGO_URI'),
      }),
      inject: [ConfigService],
    }),
    TenancyModule.forRoot({
      tenantPath: 'headers.x-tenant-id',
    }),
    CameraModule,
    SharedModule,
    DetectionModule,
    TenantModule,
    DeviceModule,
    NotificationModule,
    UserModule,
    AuthModule,
  ],
  providers: [],
})
export class AppModule {}
